#
# Author: Aseem Jakhar aseem@payatu.com
# Copyright 2012-2112 Payatu Technologies Pvt. Ltd
# website: http://www.payatu.com
#
# mmap example - map memory in the process's vitual address space 
#
# This example provide a little deeper understanding of how system calls work
#
# It demonstrates the following
#  1. mmap() system call
#     libc wrapper: ssize_t write(int fd, const void *buf, size_t count);
#  3. Setting up parameters for the system calls
#  4. Handling ASCII data
#
# NOTES:
#  1. PROT_* and MAP_* defined in 
#      <ndk-dir>/platforms/android-<api_num>/arch-arm/usr/include/asm-generic/mman.h
#  2. System call no. macros defined in
#      <ndk-dir>/platforms/android-<api_num>/arch-arm/usr/include/asm/unistd.h
#

.data

.text

#.globl _start 
.globl main 

main:
	push {lr}
	b  strt
	.ascii "\x00\xa0\x00\x00"  /* length value */
	.ascii "\x07\x00\x00\x00"  /* prot value, see PROT_* macros   */
	.ascii "\x22\x00\x00\x00"  /* flags value, see MAP_** macros  */
strt:
        mov r0, #0                 /* addr = 0 */
	ldr r1, [pc, #-24]         /* Get length value in r1 */
	ldr r2, [pc, #-24]         /* Get prot value in r2   */
	ldr r3, [pc, #-24]         /* Get flags value in r3  */
        #mov r1, #40960            /* length */
        #mov r2, #0x7              /* prot, see PROT_* macros */
        #mov r3, #0x22             /* flags, see MAP_* macros */
        mov r4, #0                 /* fd */
        mov r5, #0                 /* offset */
	mov r7, #192               /* __NR_mmap2 = 192 */
	swi #0
        mov r1, r0
        adr r0, fmt
        bl printf
        #bkpt #0
        nop
	#mov r7, #192
	#swi #0
        #mov pc, lr
	pop {pc}

fmt:
       .asciz "mmap2 returned (0x%x)\n"
       .align 4
        
