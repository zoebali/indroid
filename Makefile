#
# Author: Aseem Jakhar aseem@payatu.com
# Copyright 2012-2112 Payatu Technologies Pvt. Ltd
# website: http://www.payatu.com
#

DIR=

.PHONY: all install clean

all install clean:
	@for DIR in `ls`;                                                             \
	do                                                                            \
		if [ -d $${DIR} ];                                                    \
		then                                                                  \
			echo "";                                                      \
			echo "";                                                      \
			echo "====================================================="; \
			echo "";                                                      \
			echo "  Invoking ${MAKE} $@ for directory $${DIR}";           \
			echo "";                                                      \
			echo "====================================================="; \
			${MAKE} -C $${DIR} $@;                                        \
		fi ;                                                                  \
	done

